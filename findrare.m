function [rare_return plots_return]  = findrare(sv, cl, settings, cosm)


	%% Check cluster is allowable in survey
	if cl.redshift < sv.z_min || cl.redshift > sv.z_max
		'WARNING: Cluster is out of survey redshift range'
		%RETURN;
	elseif cl.m200m < sv.m_min || cl.m200m > sv.m_max
		'WARNING: Cluster is out of survey mass range'
		%RETURN;
	end

	%% Parameter and array initialisation
	cosm.rho_c = (2.776/cosm.h0)*10^11;
	cosm.rho_m = cosm.O_m * cosm.rho_c;

% 	%lnm_vec = linspace(log(sv.m_max), log(sv.m_min), sv.msteps);
% 	sslnm = (log(sv.m_min) - log(sv.m_max))/(sv.msteps-1);
% 	ssm = ((sv.m_min) - (sv.m_max))/(sv.msteps-1);
% 	lnm_vec = log(sv.m_max):sslnm:log(sv.m_min);
% 	m_vec = exp(lnm_vec)';
% 	
% 	%z_vec = linspace(sv.z_max, sv.z_min, sv.zsteps);
% 	ssz = (sv.z_min - (sv.z_max))/(sv.zsteps-1);
% 	z_vec = sv.z_max:ssz:sv.z_min;
	
	%lnm_vec = linspace(log(sv.m_max), log(sv.m_min), sv.msteps);
	sslnm = (log(1.e13) - log(1.e18))/(sv.msteps-1); %step size in lnM
	ssm = ((1.e13) - (1.e18))/(sv.msteps-1);
	lnm_vec = log(1.e18):sslnm:log(1.e13); %lnM vector
	m_vec = exp(lnm_vec)'; %M vector (highest mass first)
	
	%z_vec = linspace(sv.z_max, sv.z_min, sv.zsteps);
	ssz = (0.e0 - (6.e0))/(sv.zsteps-1); %step size in redshift
	z_vec = 6.e0:ssz:0.e0; %redshift vector (highest redshift first)

	[m_arr, z_arr] = ndgrid(m_vec, z_vec); %makes grids for parameter space

	growth_vec = z2growth(z_vec); %interpolates the growth function at redshift values in redshift vector
	
	if cosm.delta_sigma8 == 0  %no marginalisation over sigma8
		cosm.sigma8 = cosm.sigma8 / 0.8; %sigma8 is in units of 0.8
		cosm.Psigma8 = 1;
    else  %marginalisation over sigma8
		cosm.sigma8 = cosm.sigma8 / 0.8; %sigma8 is in units of 0.8
		cosm.sigma8bar = cosm.sigma8;
		cosm.delta_sigma8 = cosm.delta_sigma8 / 0.8;
		cosm.sigma8 = (cosm.sigma8 - 5*cosm.delta_sigma8:(10*cosm.delta_sigma8)/100:cosm.sigma8 + 5*cosm.delta_sigma8)'; %vector of sigma8s linearly around real value
		cosm.Psigma8 = normpdf(cosm.sigma8, cosm.sigma8bar, cosm.delta_sigma8); %normal probability density function
	end

	rare.nu.Rhat = ones(length(cosm.sigma8),1); 
	rare.grmgrz.Rhat = ones(length(cosm.sigma8),1);
	rare.grmdv.Rhat = ones(length(cosm.sigma8),1);

	rare.nu.m0_dsigma = ones(length(cosm.sigma8),1);
	rare.grmgrz.m0_dsigma = ones(length(cosm.sigma8),1);
	rare.grmdv.m0_dsigma = ones(length(cosm.sigma8),1);

	plots.nu.nu_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.nu.m0_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.grmgrz.m0_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.grmdv.m0_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);

	plots.nu.Pgtrm0_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.nu.Prarest_dlnmdz_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.nu.Prarest_dmdz_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);

	plots.grmgrz.Pgtrm0_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.grmgrz.Prarest_dlnmdz_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.grmgrz.Prarest_dmdz_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);

	plots.grmdv.Pgtrm0_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.grmdv.Prarest_dlnmdz_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);
	plots.grmdv.Prarest_dmdz_dsigma = ones(length(cosm.sigma8),sv.msteps, sv.zsteps);

	%% Perform calculations
	if length(cosm.sigma8)>1
		cluster_waitbar = waitbar(0,'Cluster rareness, on its way... (marginalisation over sigma_8)'); %bar for showing completion of code when marginalising over sigma8
	end
	for n=1:length(cosm.sigma8) %for every value of sigma8
		if length(cosm.sigma8)>1
			waitbar(n/length(cosm.sigma8)); %update waitbar
		end
		%% Construct cosmography
		sigma_vec = cosm.sigma8(n) * m2sigma(m_vec); %vector of sigma values corresponding to M values
		[sigma_arr growth_arr] = ndgrid(sigma_vec, growth_vec); %grid of sigma values and growth function values

		hmf.Az = 0.186*(1 + z_arr).^(-0.14); %halo mass function parameters (Tinker mass function)
		hmf.az = 1.47*(1+z_arr).^(-0.06);
		hmf.bz = 2.57*(1+z_arr).^(-0.011);
		hmf.cz = 1.19;
		hmf.fsigma = hmf.Az.*(((sigma_arr.*growth_arr)./hmf.bz).^(-hmf.az)+1).*exp(-(hmf.cz./(sigma_arr.^2.*growth_arr.^2))); %grid of halo mass function values

		dlnsdm_vec = m2dlnsdlnm(m_vec)./m_vec; %vector lnsigma derived with respect to M

		%z_intvec = linspace(0, z_vec(1), sv.zsteps);
		z_intvec = 0:z_vec(1)/(sv.zsteps-1):z_vec(1); %initial redshift vector between z=0 and first z values (for integrating comoving distance)

		H_vec = (1/(3.e3))*sqrt(cosm.O_m*(1+z_vec).^3+1-cosm.O_m); %vector of hubble parameter values (change here if not cosmological constant)
		H_intvec = (1/(3.e3))*sqrt(cosm.O_m*(1+z_intvec).^3+1-cosm.O_m); %vector of initial hubble parameter values (change here if not cosmological constant)

		arcH_intvec = trapz(z_intvec, 1./H_intvec) + cumtrapz(z_vec, 1./H_vec); %inverse hubble parameter stem function with respect to redshift, i.e. int_0^z(1/H dz') tabulated
		dvdz_vec = ((4*pi)./H_vec).*arcH_intvec.^2; %vector of comoving volume elements (fsky must be multiplied later)

		[dlnsdm_arr dvdz_arr] = ndgrid(dlnsdm_vec, dvdz_vec); %grid of dlnsdM and comoving volume element

		Ndmdv = sv.fsky*(cosm.rho_m./m_arr).*hmf.fsigma.*(-dlnsdm_arr); %number of clusters per mass interval per comoving volume
		Ndmdz = Ndmdv.*dvdz_arr; %comoving volume is converted to redshift
		Ndlnmdz = m_arr.*Ndmdz; %interval in mass is changed to interval in lnM

		preN_grmgrz = -cumtrapz(m_vec, Ndmdz); %-int_Mmax^M(dN/(dMdz) dM), i.e. number of clusters per redshift interval with mass M and higher
		N_grmgrz = -cumtrapz(z_vec, preN_grmgrz, 2); %the above is integrated along the redshift dimension, so we now have the number of clusters with mass M and higher at redshift z and higher

		N_grmdv = -cumtrapz(m_vec, Ndmdv); %see above - number density (comoving volume) of clusters with mass M and higher

		%% Draw cluster mass probability distributions
		if strcmp(settings.masserr, 'lognormal')  %Mass scatter

			cl.lnm = log(cl.m200m);
			cl.sigma_lnm = abs(log(cl.m200m_upper)-log(cl.m200m_lower));

			cl.truemass.steps = 250;
			cl.truemass.lnm_min = cl.lnm - 6*cl.sigma_lnm;
			cl.truemass.lnm_max = cl.lnm + 6*cl.sigma_lnm;

			cl.truemass.lnm_vec = linspace(cl.truemass.lnm_min, cl.truemass.lnm_max, cl.truemass.steps)';
			cl.truemass.Plnm = normpdf(cl.truemass.lnm_vec, cl.lnm, cl.sigma_lnm);
			
			cl.truemass.m_vec = exp(cl.truemass.lnm_vec);
			cl.truemass.Pm = cl.truemass.Plnm./cl.truemass.m_vec;

		elseif strcmp(settings.masserr, 'normal')

			cl.sigma_m = abs(cl.m200m_lower);

			cl.truemass.steps = 250;
			cl.truemass.m_min = cl.m200m - 6*cl.sigma_m;
			cl.truemass.m_max = cl.m200m + 6*cl.sigma_m;

			cl.truemass.m_vec = linspace(cl.truemass.m_min, cl.truemass.m_max, cl.truemass.steps)';
			cl.truemass.Pm = normpdf(cl.truemass.m_vec, cl.m200m, cl.sigma_m);
			
		end

		%% Perform Eddington bias correction if necessary
		if settings.eddington_correct == 'y'  %Eddington bias

			if strcmp(settings.masserr, 'lognormal')

				cl.obsmass.lnm_min = cl.lnm - 8*cl.sigma_lnm;
				cl.obsmass.lnm_max = cl.lnm + 8*cl.sigma_lnm;
				cl.obsmass.steps = 300;
				cl.obsmass.lnm_vec = linspace(cl.obsmass.lnm_min, cl.obsmass.lnm_max, cl.obsmass.steps);

				cl.obsmass.Plnm = normpdf(cl.obsmass.lnm_vec, cl.lnm, cl.sigma_lnm)';

				cl.truemass.Plnm = exp(cl.obsmass.lnm_vec)'.*interp2(z_vec, m_vec, Ndmdz, cl.redshift, exp(cl.obsmass.lnm_vec)).*cl.obsmass.Plnm;
				cl.truemass.Plnm(~isfinite(cl.truemass.Plnm)) = 0;
				cl.truemass.Plnm = cl.truemass.Plnm/trapz(cl.obsmass.lnm_vec, cl.truemass.Plnm);
				cl.truemass.lnm_vec = cl.obsmass.lnm_vec';

				cl.truemass.m_vec = exp(cl.truemass.lnm_vec);
				cl.truemass.Pm = cl.truemass.Plnm./cl.truemass.m_vec;

			elseif strcmp(settings.masserr, 'normal')

				cl.obsmass.m_min = cl.m200m - 8*cl.sigma_m;
				cl.obsmass.m_max = cl.m200m + 8*cl.sigma_m;
				cl.obsmass.steps = 300;
				cl.obsmass.m_vec = linspace(cl.obsmass.m_min, cl.obsmass.m_max, cl.obsmass.steps);

				cl.obsmass.Pm = normpdf(cl.obsmass.m_vec, cl.m200m, cl.sigma_m)';

				cl.truemass.Pm = interp2(z_vec,m_vec,Ndmdz,cl.redshift,cl.obsmass.m_vec).*cl.obsmass.Pm;
				cl.truemass.Pm(~isfinite(cl.truemass.Pm)) = 0;
				cl.truemass.Pm = cl.truemass.Pm/trapz(cl.obsmass.m_vec, cl.truemass.Pm);
				cl.truemass.m_vec = cl.obsmass.m_vec';

			end

		end

		%% Calculate extremeness values
		cl.nu_obs = 1./(interp1(z_vec, growth_vec, cl.redshift)*cosm.sigma8(n)*m2sigma(cl.truemass.m_vec)); %calculate nu as 1/(growth*sigma) (3.2)
		cl.N_grmgrz_obs = interp2(z_arr, m_arr, N_grmgrz, cl.redshift, cl.truemass.m_vec); %Number of clusters with mass greater than the observed at redshifts greater than the observed (3.1)
		cl.N_grmdv_obs = interp2(z_arr, m_arr, N_grmdv, cl.redshift, cl.truemass.m_vec); %comoving number density of clusters with masses greater than the observed (3.3)

		%% Calculate equivalent masses at z=0
		cl.m0.nudm = interp1(1./(sigma_vec), m_vec, cl.nu_obs); %mass of cluster with same nu as observed but at redshift zero (growth(z=0) = 1)
		cl.m0.nudm(~isfinite(cl.m0.nudm)) = 0; %if above returns infinity of any sort - the mass is forced to zero
		cl.m0.nudsigma8(n) = trapz(cl.truemass.m_vec, cl.m0.nudm.*cl.truemass.Pm); %correction for mass scatter and eddington bias

		cl.m0.grmgrzdm = interp1(N_grmgrz(sum([N_grmgrz(1:end,end)==0]):end,end),m_vec(sum([N_grmgrz(1:end,end)==0]):end),cl.N_grmgrz_obs); %mass of cluster with same number of more massive cluster and higher redshift as observed but at redshift zero (look at this)
		cl.m0.grmgrzdm(~isfinite(cl.m0.grmgrzdm)) = 0;
		cl.m0.grmgrzdsigma8(n) = trapz(cl.truemass.m_vec, cl.m0.grmgrzdm.*cl.truemass.Pm);

		cl.m0.grmdvdm = interp1(N_grmdv(sum([N_grmdv(1:end,end)==0]):end,end),m_vec(sum([N_grmdv(1:end,end)==0]):end),cl.N_grmdv_obs); %mass of cluster with same comoving number density of more massive cluster as observed but at redshift zero (look at this)
		cl.m0.grmdvdm(~isfinite(cl.m0.grmdvdm)) = 0;
		cl.m0.grmdvdsigma8(n) = trapz(cl.truemass.m_vec, cl.m0.grmdvdm.*cl.truemass.Pm);

		%% Calculate expected numbers of equal or less extreme clusters in the
		%% survey
		Ndmdz(z_arr>sv.z_max | z_arr<sv.z_min | m_arr < sv.m_min) = 0; %enforce survey window in mass and redshift

		for l=1:length(cl.truemass.m_vec)

			rare.nu.Ndmdz=Ndmdz;
			rare.nu.Ndmdz(1./(growth_arr.*sigma_arr)<cl.nu_obs(l)) = 0; %remove all more extreme clusters according to nu parameter
			rare.nu.preN_grnu = trapz(m_vec, rare.nu.Ndmdz);
			rare.nu.N_grnu(l) = trapz(z_vec, rare.nu.preN_grnu); %integrated over mass and redshift, now contains the number equal or less extreme clusters

			rare.grmgrz.Ndmdz=Ndmdz;
			rare.grmgrz.Ndmdz(N_grmgrz>cl.N_grmgrz_obs(l)) = 0; %remove all more extreme clusters according to number of more massive clusters
			rare.grmgrz.preN_ltgrmgrz = trapz(m_vec, rare.grmgrz.Ndmdz);
			rare.grmgrz.N_ltgrmgrz(l) = trapz(z_vec, rare.grmgrz.preN_ltgrmgrz); %integrated over mass and redshift, now contains the number equal or less extreme clusters

			rare.grmdv.Ndmdz=Ndmdz;
			rare.grmdv.Ndmdz(N_grmdv>cl.N_grmdv_obs(l)) = 0; %remove all more extreme clusters according to comoving number density of more massive clusters
			rare.grmdv.preN_ltgrmdv = trapz(m_vec, rare.grmdv.Ndmdz);
			rare.grmdv.N_ltgrmdv(l) = trapz(z_vec, rare.grmdv.preN_ltgrmdv); %integrated over mass and redshift, now contains the number equal or less extreme clusters

		end

		rare.nu.Pdm = 1-exp(-rare.nu.N_grnu); %Probability of detecting a cluster that is more rare than the observed
		rare.grmgrz.Pdm = 1-exp(-rare.grmgrz.N_ltgrmgrz);
		rare.grmdv.Pdm = 1-exp(-rare.grmdv.N_ltgrmdv);

		rare.nu.Rhatdsigma8(n) = trapz(cl.truemass.m_vec, rare.nu.Pdm'.*cl.truemass.Pm); %convolution with the probability of massuring a certain mass
		rare.grmgrz.Rhatdsigma8(n) = trapz(cl.truemass.m_vec, rare.grmgrz.Pdm'.*cl.truemass.Pm);
		rare.grmdv.Rhatdsigma8(n) = trapz(cl.truemass.m_vec, rare.grmdv.Pdm'.*cl.truemass.Pm);
		
		if settings.makeplots == 'y'
		
			%% Calculate meshes for m0 contour plots
			plots.nu.nu_dsigma(n,:,:) = cosm.Psigma8(n)./(growth_arr.*sigma_arr);
			plots.nu.m0_dsigma(n,:,:) = cosm.Psigma8(n)*interp1(sigma_vec,m_vec,cosm.Psigma8(n)./plots.nu.nu_dsigma(n,:,:));
			plots.grmgrz.m0_dsigma(n,:,:) = cosm.Psigma8(n)*interp1(N_grmgrz(sum([N_grmgrz(1:end,end)==0]):end,end),m_vec(sum([N_grmgrz(1:end,end)==0]):end),N_grmgrz);
			plots.grmdv.m0_dsigma(n,:,:) = cosm.Psigma8(n)*interp1(N_grmdv(sum([N_grmdv(1:end,end)==0]):end,end),m_vec(sum([N_grmdv(1:end,end)==0]):end),N_grmdv);

			%% Calculate meshes for Rhat contour plots
			
			% nu measure
			m0_nu_min = min(min(plots.nu.m0_dsigma(n,:,:)/cosm.Psigma8(n)));
			m0_nu_max = max(max(plots.nu.m0_dsigma(n,:,:)/cosm.Psigma8(n)));

			m0_nusteps=100;
			lnm0_nu_vec = linspace(log(m0_nu_max), log(m0_nu_min), m0_nusteps);
			m0_nu_vec = exp(lnm0_nu_vec)';

			Pzrgtrm0_nu=ones(m0_nusteps,1);

			for l=1:m0_nusteps

				N_gtrm0_nu_dmdz=Ndmdz;
				N_gtrm0_nu_dmdz((plots.nu.m0_dsigma(n,:,:)/cosm.Psigma8(n))<m0_nu_vec(l))=0;
				N_gtrm0_nu_dmdz(z_arr>sv.z_max | z_arr<sv.z_min | m_arr<sv.m_min)=0;

				preN_gtrm0_nu=trapz(m_vec,N_gtrm0_nu_dmdz);
				N_gtrm0_nu=trapz(z_vec,preN_gtrm0_nu);
				Pzrgtrm0_nu(l)=exp(-N_gtrm0_nu);

			end

			plots.nu.prePgtrm0=interp1(m0_nu_vec,Pzrgtrm0_nu,plots.nu.m0_dsigma(n,:,:)/cosm.Psigma8(n));
			plots.nu.Pgtrm0(:,:)=plots.nu.prePgtrm0(1,:,:);
			plots.nu.Pgtrm0_dsigma(n,:,:)=plots.nu.Pgtrm0(:,:);
			plots.nu.Prarest_dlnmdz_dsigma(n,:,:)=cosm.Psigma8(n)*(1-exp(-Ndlnmdz*ssz*sslnm)).*plots.nu.Pgtrm0;
			plots.nu.Prarest_dmdz_dsigma(n,:,:)=cosm.Psigma8(n)*(1-exp(-Ndmdz*ssz*ssm)).*plots.nu.Pgtrm0;
			
			% >m>z measure

			m0_grmgrz_min=min(min(plots.grmgrz.m0_dsigma(n,:,:)/cosm.Psigma8(n)));
			m0_grmgrz_max=max(max(plots.grmgrz.m0_dsigma(n,:,:)/cosm.Psigma8(n)));

			m0_grmgrzsteps=100;
			lnm0_grmgrz_vec = linspace(log(m0_grmgrz_max), log(m0_grmgrz_min), m0_grmgrzsteps);
			m0_grmgrz_vec = exp(lnm0_grmgrz_vec)';

			Pzrgtrm0_grmgrz=ones(m0_grmgrzsteps,1);

			for l=1:m0_grmgrzsteps

				N_gtrm0_grmgrz_dmdz=Ndmdz;
				N_gtrm0_grmgrz_dmdz((plots.grmgrz.m0_dsigma(n,:,:)/cosm.Psigma8(n))<m0_grmgrz_vec(l))=0;
				N_gtrm0_grmgrz_dmdz(z_arr>sv.z_max | z_arr<sv.z_min | m_arr<sv.m_min)=0;

				preN_gtrm0_grmgrz=trapz(m_vec,N_gtrm0_grmgrz_dmdz);
				N_gtrm0_grmgrz=trapz(z_vec,preN_gtrm0_grmgrz);
				Pzrgtrm0_grmgrz(l)=exp(-N_gtrm0_grmgrz);

			end

			plots.grmgrz.prePgtrm0=interp1(m0_grmgrz_vec,Pzrgtrm0_grmgrz,plots.grmgrz.m0_dsigma(n,:,:)/cosm.Psigma8(n));
			plots.grmgrz.Pgtrm0(:,:)=plots.grmgrz.prePgtrm0(1,:,:);
			plots.grmgrz.Pgtrm0_disgma(n,:,:)=plots.grmgrz.prePgtrm0(1,:,:);
			plots.grmgrz.Prarest_dlnmdz_dsigma(n,:,:)=cosm.Psigma8(n)*(1-exp(-Ndlnmdz*ssz*sslnm)).*plots.grmgrz.Pgtrm0;
			plots.grmgrz.Prarest_dmdz_dsigma(n,:,:)=cosm.Psigma8(n)*(1-exp(-Ndmdz*ssz*ssm)).*plots.grmgrz.Pgtrm0;

			% >mdv measure

			m0_grmdv_min=min(min(plots.grmdv.m0_dsigma(n,:,:)/cosm.Psigma8(n)));
			m0_grmdv_max=max(max(plots.grmdv.m0_dsigma(n,:,:)/cosm.Psigma8(n)));

			m0_grmdvsteps=100;
			lnm0_grmdv_vec = linspace(log(m0_grmdv_max), log(m0_grmdv_min), m0_grmdvsteps);
			m0_grmdv_vec = exp(lnm0_grmdv_vec)';

			Pzrgtrm0_grmdv=ones(m0_grmdvsteps,1);

			for l=1:m0_grmdvsteps

				N_gtrm0_grmdv_dmdz=Ndmdz;
				N_gtrm0_grmdv_dmdz((plots.grmdv.m0_dsigma(n,:,:)/cosm.Psigma8(n))<m0_grmdv_vec(l))=0;
				N_gtrm0_grmdv_dmdz(z_arr>sv.z_max | z_arr<sv.z_min | m_arr<sv.m_min)=0;

				preN_gtrm0_grmdv=trapz(m_vec,N_gtrm0_grmdv_dmdz);
				N_gtrm0_grmdv=trapz(z_vec,preN_gtrm0_grmdv);
				Pzrgtrm0_grmdv(l)=exp(-N_gtrm0_grmdv);

			end

			plots.grmdv.prePgtrm0=interp1(m0_grmdv_vec,Pzrgtrm0_grmdv,plots.grmdv.m0_dsigma(n,:,:)/cosm.Psigma8(n));
			plots.grmdv.Pgtrm0(:,:)=plots.grmdv.prePgtrm0(1,:,:);
			plots.grmdv.Pgtrm0_dsigma(n,:,:)=plots.grmdv.prePgtrm0(1,:,:);
			plots.grmdv.Prarest_dlnmdz_dsigma(n,:,:)=cosm.Psigma8(n)*(1-exp(-Ndlnmdz*ssz*sslnm)).*plots.grmdv.Pgtrm0;
			plots.grmdv.Prarest_dmdz_dsigma(n,:,:)=cosm.Psigma8(n)*(1-exp(-Ndmdz*ssz*ssm)).*plots.grmdv.Pgtrm0;
			
		end
		

	end
	if length(cosm.sigma8)>1
		close(cluster_waitbar);
	end

	%% Marginalise over sigma8
	if length(cosm.sigma8)>1
		rare.nu.Rhat = trapz(cosm.sigma8, rare.nu.Rhatdsigma8'.*cosm.Psigma8);
		rare.grmgrz.Rhat = trapz(cosm.sigma8, rare.grmgrz.Rhatdsigma8'.*cosm.Psigma8);
		rare.grmdv.Rhat = trapz(cosm.sigma8, rare.grmdv.Rhatdsigma8'.*cosm.Psigma8);

		rare.nu.m0 = trapz(cosm.sigma8, cl.m0.nudsigma8'.*cosm.Psigma8);
		rare.grmgrz.m0 = trapz(cosm.sigma8, cl.m0.grmgrzdsigma8'.*cosm.Psigma8);
		rare.grmdv.m0 = trapz(cosm.sigma8, cl.m0.grmdvdsigma8'.*cosm.Psigma8);
		
		plots.nu.nu = trapz(cosm.sigma8, plots.nu.nu_dsigma);
		plots.nu.m0 = trapz(cosm.sigma8, plots.nu.m0_dsigma);
		plots.grmgrz.m0 = trapz(cosm.sigma8, plots.grmgrz.m0_dsigma);
		plots.grmdv.m0 = trapz(cosm.sigma8, plots.grmdv.m0_dsigma);
		
		plots.nu.Pgtrm0 = trapz(cosm.sigma8, plots.nu.Pgtrm0_dsigma);
		plots.nu.Prarest_dlnmdz = trapz(cosm.sigma8, plots.nu.Prarest_dlnmdz_dsigma);
		plots.nu.Prarest_dmdz = trapz(cosm.sigma8, plots.nu.Prarest_dmdz_dsigma);
		
		plots.grmgrz.Pgtrm0 = trapz(cosm.sigma8, plots.grmgrz.Pgtrm0_dsigma);
		plots.grmgrz.Prarest_dlnmdz = trapz(cosm.sigma8, plots.grmgrz.Prarest_dlnmdz_dsigma);
		plots.grmgrz.Prarest_dmdz = trapz(cosm.sigma8, plots.grmgrz.Prarest_dmdz_dsigma);
		
		plots.grmdv.Pgtrm0 = trapz(cosm.sigma8, plots.grmdv.Pgtrm0_dsigma);
		plots.grmdv.Prarest_dlnmdz = trapz(cosm.sigma8, plots.grmdv.Prarest_dlnmdz_dsigma);
		plots.grmdv.Prarest_dmdz = trapz(cosm.sigma8, plots.grmdv.Prarest_dmdz_dsigma);
		
	else
		rare.nu.Rhat = rare.nu.Rhatdsigma8;
		rare.grmgrz.Rhat = rare.grmgrz.Rhatdsigma8;
		rare.grmdv.Rhat = rare.grmdv.Rhatdsigma8;
		
		rare.nu.m0 = cl.m0.nudsigma8;
		rare.grmgrz.m0 = cl.m0.grmgrzdsigma8;
		rare.grmdv.m0 = cl.m0.grmdvdsigma8;
		
		plots.nu.nu = plots.nu.nu_dsigma;
		plots.nu.m0 = plots.nu.m0_dsigma;
		plots.grmgrz.m0 = plots.grmgrz.m0_dsigma;
		plots.grmdv.m0 = plots.grmdv.m0_dsigma;
		
		plots.nu.Pgtrm0 = plots.nu.Pgtrm0_dsigma;
		plots.nu.Prarest_dlnmdz = plots.nu.Prarest_dlnmdz_dsigma;
		plots.nu.Prarest_dmdz = plots.nu.Prarest_dmdz_dsigma;
		
		plots.grmgrz.Pgtrm0 = plots.grmgrz.Pgtrm0_dsigma;
		plots.grmgrz.Prarest_dlnmdz = plots.grmgrz.Prarest_dlnmdz_dsigma;
		plots.grmgrz.Prarest_dmdz = plots.grmgrz.Prarest_dmdz_dsigma;
		
		plots.grmdv.Pgtrm0 = plots.grmdv.Pgtrm0_dsigma;
		plots.grmdv.Prarest_dlnmdz = plots.grmdv.Prarest_dlnmdz_dsigma;
		plots.grmdv.Prarest_dmdz = plots.grmdv.Prarest_dmdz_dsigma;
	end
	
	%% Create structs to return to the user
	rare_return.nu.m0 = rare.nu.m0;
	rare_return.grmgrz.m0 = rare.grmgrz.m0;
	rare_return.grmdv.m0 = rare.grmdv.m0;
	
	rare_return.nu.Rhat = rare.nu.Rhat;
	rare_return.grmgrz.Rhat = rare.grmgrz.Rhat;
	rare_return.grmdv.Rhat = rare.grmdv.Rhat;
	
	plots_return.nu.m0.data(:,:) = plots.nu.m0(1,:,:);
	plots_return.grmgrz.m0.data(:,:) = plots.grmgrz.m0(1,:,:);
	plots_return.grmdv.m0.data(:,:) = plots.grmdv.m0(1,:,:);
	
	plots_return.nu.Prarest.data(:,:) = plots.nu.Prarest_dmdz(1,:,:);
	plots_return.grmgrz.Prarest.data(:,:) = plots.grmgrz.Prarest_dmdz(1,:,:);
	plots_return.grmdv.Prarest.data(:,:) = plots.grmdv.Prarest_dmdz(1,:,:);
	
	plots_return.nu.Pgtrm0.data(:,:) = plots.nu.Pgtrm0(1,:,:);
	plots_return.grmgrz.Pgtrm0.data(:,:) = plots.grmgrz.Pgtrm0(1,:,:);
	plots_return.grmdv.Pgtrm0.data(:,:) = plots.grmdv.Pgtrm0(1,:,:);
	
	plots_return.z_arr = z_arr;
	plots_return.m_arr = m_arr;
	plots_return.m_plot = cl.truemass.m_vec(cl.truemass.Pm==max(cl.truemass.Pm));
	plots_return.m_plot = mean(plots_return.m_plot);

%% Nested functions for speedy calculation from here onwards...

    function sigM=m2sigma(Mass)
    
   [logMsigM]=[41.4465    0.0485
	   41.3538    0.0506
	   41.2611    0.0529
	   41.1684    0.0552
	   41.0756    0.0576
	   40.9829    0.0601
	   40.8902    0.0626
	   40.7975    0.0653
	   40.7048    0.0680
	   40.6120    0.0708
	   40.5193    0.0738
	   40.4266    0.0768
	   40.3339    0.0799
	   40.2412    0.0831
	   40.1484    0.0864
	   40.0557    0.0899
	   39.9630    0.0934
	   39.8703    0.0970
	   39.7775    0.1008
	   39.6848    0.1047
	   39.5921    0.1087
	   39.4994    0.1128
	   39.4067    0.1170
	   39.3139    0.1214
	   39.2212    0.1259
	   39.1285    0.1305
	   39.0358    0.1352
	   38.9430    0.1401
	   38.8503    0.1451
	   38.7576    0.1503
	   38.6649    0.1555
	   38.5722    0.1610
	   38.4794    0.1665
	   38.3867    0.1723
	   38.2940    0.1781
	   38.2013    0.1841
	   38.1086    0.1903
	   38.0158    0.1967
	   37.9231    0.2032
	   37.8304    0.2098
	   37.7377    0.2166
	   37.6449    0.2236
	   37.5522    0.2308
	   37.4595    0.2382
	   37.3668    0.2457
	   37.2741    0.2534
	   37.1813    0.2612
	   37.0886    0.2693
	   36.9959    0.2775
	   36.9032    0.2859
	   36.8105    0.2944
	   36.7177    0.3032
	   36.6250    0.3122
	   36.5323    0.3213
	   36.4396    0.3306
	   36.3468    0.3401
	   36.2541    0.3499
	   36.1614    0.3598
	   36.0687    0.3698
	   35.9760    0.3801
	   35.8832    0.3906
	   35.7905    0.4013
	   35.6978    0.4122
	   35.6051    0.4232
	   35.5124    0.4345
	   35.4196    0.4460
	   35.3269    0.4576
	   35.2342    0.4695
	   35.1415    0.4815
	   35.0487    0.4937
	   34.9560    0.5061
	   34.8633    0.5188
	   34.7706    0.5316
	   34.6779    0.5446
	   34.5851    0.5578
	   34.4924    0.5712
	   34.3997    0.5848
	   34.3070    0.5986
	   34.2143    0.6126
	   34.1215    0.6268
	   34.0288    0.6411
	   33.9361    0.6557
	   33.8434    0.6705
	   33.7506    0.6855
	   33.6579    0.7007
	   33.5652    0.7160
	   33.4725    0.7316
	   33.3798    0.7474
	   33.2870    0.7633
	   33.1943    0.7795
	   33.1016    0.7958
	   33.0089    0.8124
	   32.9161    0.8291
	   32.8234    0.8460
	   32.7307    0.8631
	   32.6380    0.8804
	   32.5453    0.8979
	   32.4525    0.9156
	   32.3598    0.9335
	   32.2671    0.9516
	   32.1744    0.9698
	   32.0817    0.9883
	   31.9889    1.0069
	   31.8962    1.0258
	   31.8035    1.0448
	   31.7108    1.0640
	   31.6180    1.0834
	   31.5253    1.1030
	   31.4326    1.1228
	   31.3399    1.1427
	   31.2472    1.1629
	   31.1544    1.1832
	   31.0617    1.2038
	   30.9690    1.2245
	   30.8763    1.2454
	   30.7836    1.2665
	   30.6908    1.2877
	   30.5981    1.3092
	   30.5054    1.3308
	   30.4127    1.3527
	   30.3199    1.3747
	   30.2272    1.3969
	   30.1345    1.4192
	   30.0418    1.4418
	   29.9491    1.4645
	   29.8563    1.4874
	   29.7636    1.5105
	   29.6709    1.5338
	   29.5782    1.5572
	   29.4855    1.5809
	   29.3927    1.6046
	   29.3000    1.6286
	   29.2073    1.6528
	   29.1146    1.6771
	   29.0218    1.7016
	   28.9291    1.7263
	   28.8364    1.7511
	   28.7437    1.7761
	   28.6510    1.8013
	   28.5582    1.8267
	   28.4655    1.8522
	   28.3728    1.8780
	   28.2801    1.9038
	   28.1874    1.9299
	   28.0946    1.9561
	   28.0019    1.9825
	   27.9092    2.0091
	   27.8165    2.0358
	   27.7237    2.0627
	   27.6310    2.0898];
                
        %Fudge for normally distributed cluster error masses to prevent problem with log(Mass<0)
		Mass(Mass<0)=1.e10;
            
        sigM=interp1(logMsigM(:,1),logMsigM(:,2),log(Mass),'spline');
    
    end
        
    function dlnsigdlnM=m2dlnsdlnm(Mass)
       
        [logMdlnsigdlnM]=[41.4465 -0.4687
	   41.3538   -0.4687
	   41.2611   -0.4648
	   41.1684   -0.4610
	   41.0756   -0.4570
	   40.9829   -0.4531
	   40.8902   -0.4492
	   40.7975   -0.4454
	   40.7048   -0.4416
	   40.6120   -0.4379
	   40.5193   -0.4342
	   40.4266   -0.4306
	   40.3339   -0.4272
	   40.2412   -0.4238
	   40.1484   -0.4206
	   40.0557   -0.4174
	   39.9630   -0.4144
	   39.8703   -0.4114
	   39.7775   -0.4084
	   39.6848   -0.4054
	   39.5921   -0.4024
	   39.4994   -0.3993
	   39.4067   -0.3962
	   39.3139   -0.3931
	   39.2212   -0.3900
	   39.1285   -0.3868
	   39.0358   -0.3836
	   38.9430   -0.3805
	   38.8503   -0.3773
	   38.7576   -0.3742
	   38.6649   -0.3712
	   38.5722   -0.3682
	   38.4794   -0.3654
	   38.3867   -0.3626
	   38.2940   -0.3599
	   38.2013   -0.3572
	   38.1086   -0.3546
	   38.0158   -0.3520
	   37.9231   -0.3494
	   37.8304   -0.3468
	   37.7377   -0.3442
	   37.6449   -0.3416
	   37.5522   -0.3389
	   37.4595   -0.3362
	   37.3668   -0.3336
	   37.2741   -0.3309
	   37.1813   -0.3282
	   37.0886   -0.3256
	   36.9959   -0.3229
	   36.9032   -0.3203
	   36.8105   -0.3176
	   36.7177   -0.3150
	   36.6250   -0.3124
	   36.5323   -0.3099
	   36.4396   -0.3073
	   36.3468   -0.3048
	   36.2541   -0.3023
	   36.1614   -0.2998
	   36.0687   -0.2972
	   35.9760   -0.2947
	   35.8832   -0.2922
	   35.7905   -0.2896
	   35.6978   -0.2871
	   35.6051   -0.2845
	   35.5124   -0.2819
	   35.4196   -0.2794
	   35.3269   -0.2768
	   35.2342   -0.2743
	   35.1415   -0.2717
	   35.0487   -0.2692
	   34.9560   -0.2668
	   34.8633   -0.2643
	   34.7706   -0.2619
	   34.6779   -0.2595
	   34.5851   -0.2572
	   34.4924   -0.2549
	   34.3997   -0.2526
	   34.3070   -0.2503
	   34.2143   -0.2481
	   34.1215   -0.2459
	   34.0288   -0.2437
	   33.9361   -0.2415
	   33.8434   -0.2394
	   33.7506   -0.2372
	   33.6579   -0.2351
	   33.5652   -0.2330
	   33.4725   -0.2309
	   33.3798   -0.2288
	   33.2870   -0.2268
	   33.1943   -0.2248
	   33.1016   -0.2228
	   33.0089   -0.2208
	   32.9161   -0.2188
	   32.8234   -0.2169
	   32.7307   -0.2150
	   32.6380   -0.2132
	   32.5453   -0.2113
	   32.4525   -0.2095
	   32.3598   -0.2077
	   32.2671   -0.2059
	   32.1744   -0.2042
	   32.0817   -0.2024
	   31.9889   -0.2007
	   31.8962   -0.1990
	   31.8035   -0.1974
	   31.7108   -0.1957
	   31.6180   -0.1941
	   31.5253   -0.1925
	   31.4326   -0.1909
	   31.3399   -0.1893
	   31.2472   -0.1878
	   31.1544   -0.1863
	   31.0617   -0.1848
	   30.9690   -0.1833
	   30.8763   -0.1818
	   30.7836   -0.1804
	   30.6908   -0.1789
	   30.5981   -0.1775
	   30.5054   -0.1761
	   30.4127   -0.1748
	   30.3199   -0.1734
	   30.2272   -0.1720
	   30.1345   -0.1707
	   30.0418   -0.1694
	   29.9491   -0.1681
	   29.8563   -0.1668
	   29.7636   -0.1655
	   29.6709   -0.1642
	   29.5782   -0.1630
	   29.4855   -0.1618
	   29.3927   -0.1605
	   29.3000   -0.1593
	   29.2073   -0.1582
	   29.1146   -0.1570
	   29.0218   -0.1558
	   28.9291   -0.1547
	   28.8364   -0.1536
	   28.7437   -0.1525
	   28.6510   -0.1514
	   28.5582   -0.1503
	   28.4655   -0.1492
	   28.3728   -0.1482
	   28.2801   -0.1471
	   28.1874   -0.1461
	   28.0946   -0.1451
	   28.0019   -0.1441
	   27.9092   -0.1431
	   27.8165   -0.1421
	   27.7237   -0.1411
	   27.6310   -0.1411];
        
		dlnsigdlnM=interp1(logMdlnsigdlnM(:,1),logMdlnsigdlnM(:,2),log(Mass),'spline');
        
    end

    function Dzvec=z2growth(zvec)
        
      zedDzed=[ 6.000000000000000000e+00 1.863302967387182318e-01
5.939393939393939448e+00 1.879577816511592558e-01
5.878787878787878896e+00 1.896136978490663150e-01
5.818181818181818343e+00 1.912987923772922483e-01
5.757575757575757791e+00 1.930138384776890759e-01
5.696969696969697239e+00 1.947596367361626724e-01
5.636363636363636687e+00 1.965370162897773498e-01
5.575757575757576134e+00 1.983468360975587375e-01
5.515151515151515582e+00 2.001899862788827922e-01
5.454545454545455030e+00 2.020673895236153317e-01
5.393939393939394478e+00 2.039800025784533344e-01
5.333333333333333925e+00 2.059288178142296422e-01
5.272727272727272485e+00 2.079148648792789322e-01
5.212121212121211933e+00 2.099392124443245911e-01
5.151515151515151381e+00 2.120029700447364218e-01
5.090909090909090828e+00 2.141072900264279211e-01
5.030303030303030276e+00 2.162533696021161078e-01
4.969696969696969724e+00 2.184424530251553032e-01
4.909090909090909172e+00 2.206758338886878890e-01
4.848484848484848619e+00 2.229548575584174297e-01
4.787878787878788067e+00 2.252809237479382054e-01
4.727272727272727515e+00 2.276554892462059332e-01
4.666666666666666963e+00 2.300800708074683676e-01
4.606060606060606410e+00 2.325562482147416621e-01
4.545454545454545858e+00 2.350856675287660291e-01
4.484848484848485306e+00 2.376700445352805946e-01
4.424242424242424754e+00 2.403111684044429985e-01
4.363636363636363313e+00 2.430109055772838866e-01
4.303030303030302761e+00 2.457712038952366285e-01
4.242424242424242209e+00 2.485940969900320430e-01
4.181818181818181657e+00 2.514817089525894778e-01
4.121212121212121104e+00 2.544362593010018880e-01
4.060606060606060552e+00 2.574600682692821940e-01
4.000000000000000000e+00 2.605555624402460246e-01
3.939393939393939448e+00 2.637252807477465377e-01
3.878787878787878896e+00 2.669718808754688588e-01
3.818181818181818343e+00 2.702981460816359371e-01
3.757575757575757791e+00 2.737069924812939870e-01
3.696969696969697239e+00 2.772014768203440171e-01
3.636363636363636687e+00 2.807848047781651979e-01
3.575757575757575690e+00 2.844603398385647797e-01
3.515151515151515138e+00 2.882316127718798637e-01
3.454545454545454586e+00 2.921023317743662306e-01
3.393939393939394034e+00 2.960763933145458826e-01
3.333333333333333481e+00 3.001578937399358438e-01
3.272727272727272929e+00 3.043511417015740128e-01
3.212121212121212377e+00 3.086606714579470667e-01
3.151515151515151381e+00 3.130912571243413267e-01
3.090909090909090828e+00 3.176479279382154464e-01
3.030303030303030276e+00 3.223359846159313813e-01
2.969696969696969724e+00 3.271610168809999108e-01
2.909090909090909172e+00 3.321289222488475468e-01
2.848484848484848619e+00 3.372459261578686918e-01
2.787878787878788067e+00 3.425186035410942020e-01
2.727272727272727515e+00 3.479539019369521924e-01
2.666666666666666963e+00 3.535591662411556313e-01
2.606060606060605966e+00 3.593421652043541914e-01
2.545454545454545414e+00 3.653111197814972733e-01
2.484848484848484862e+00 3.714747334383451949e-01
2.424242424242424310e+00 3.778422245175934169e-01
2.363636363636363757e+00 3.844233607608557590e-01
2.303030303030303205e+00 3.912284960722214877e-01
2.242424242424242653e+00 3.982686095929799386e-01
2.181818181818181657e+00 4.055553471337514648e-01
2.121212121212121104e+00 4.131010649776221677e-01
2.060606060606060552e+00 4.209188760233646409e-01
2.000000000000000000e+00 4.290226981782495064e-01
1.939393939393939448e+00 4.374273048313234158e-01
1.878787878787878896e+00 4.461483771353791927e-01
1.818181818181818343e+00 4.552025576931450490e-01
1.757575757575757569e+00 4.646075050729445155e-01
1.696969696969697017e+00 4.743819483621831701e-01
1.636363636363636465e+00 4.845457406924544297e-01
1.575757575757575690e+00 4.951199103246549660e-01
1.515151515151515138e+00 5.061267074506058394e-01
1.454545454545454586e+00 5.175896443310314954e-01
1.393939393939394034e+00 5.295335257274487173e-01
1.333333333333333481e+00 5.419844657742721727e-01
1.272727272727272707e+00 5.549698864522931707e-01
1.212121212121212155e+00 5.685184916404568689e-01
1.151515151515151603e+00 5.826602093167706498e-01
1.090909090909090828e+00 5.974260928348888999e-01
1.030303030303030276e+00 6.128481703170555184e-01
9.696969696969697239e-01 6.289592290951987463e-01
9.090909090909091717e-01 6.457925198529822497e-01
8.484848484848485084e-01 6.633813627744484798e-01
7.878787878787878451e-01 6.817586357553787613e-01
7.272727272727272929e-01 7.009561228179316261e-01
6.666666666666667407e-01 7.210036995764005363e-01
6.060606060606060774e-01 7.419283321951535282e-01
5.454545454545454142e-01 7.637528667949352590e-01
4.848484848484848619e-01 7.864945870802492189e-01
4.242424242424242542e-01 8.101635170071842529e-01
3.636363636363636465e-01 8.347604378042565720e-01
3.030303030303030387e-01 8.602745655662096258e-01
2.424242424242424310e-01 8.866807830537837276e-01
1.818181818181818232e-01 9.139362225144676000e-01
1.212121212121212155e-01 9.419758577800598776e-01
6.060606060606060774e-02 9.707066544233607930e-01
0.000000000000000000e+00 1.000000000000000000e+00]; 
		 
     Dzvec=interp1(zedDzed(:,1),zedDzed(:,2),zvec,'spline');
     
    end

end
