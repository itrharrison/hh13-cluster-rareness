%% EXAMPLE SCRIPT FOR CALCULATING THE RARENESS OF HIGH MASS GALAXY CLUSTERS
% USING THE METHODOLOGY OF HARRISON & HOTCHKISS arXiv:1210.4369
% IF YOU USE THIS SCRIPT TO DO SOME USEFUL SCIENCE WHICH YOU THEN PUBLISH
% PLEASE CITE H&H arXiv:1210.4369
% (BASED ON THE IDEA OF MORTONSON, HU & HUTERER arXiv:1011.0004 WITH THE CORRECTIONS SUGGESTED BY
% HOTCHKISS arXiv:1105.3630)

%% Specify cosmology
%% PLEASE NOTE: sigma(m) and linear growth(z) are interpolated from pre-computed tables using the WMAP7 cosmological parameters
cosmology.h0 = 0.703;
cosmology.O_m = 0.274;
cosmology.sigma8 = 0.811;
cosmology.delta_sigma8 = 0.00; %0.03 % Set to 0 to not marginalise over sigma_8 (and save yourself time...)

%% Specify survey
%% For lower limits on rareness (upper limits on tension) specify the region in which your survey is fully complete
survey.z_min = 0.0e0;
survey.z_max = 2.0e0;
survey.m_min = 1.e14; % in units of h^-1
survey.m_max = 1.e18; % in units of h^-1
survey.fsky = 41253/41253;
survey.msteps = 301; %integration resolution
survey.zsteps = 300;

%% Specify cluster
cluster.m = 8.93e14; % Cluster mass ***in units of h^-1***
cluster.m_lower = 1.48e14; % Lower error bar on cluster mass ***in units of h^-1***
cluster.m_upper = 1.48e14; % Upper error bar on cluster mass ***in units of h^-1***
cluster.rhotype = 'm'; % Overdensity type cluster mass defined wrt. 'm' for average, 'c' for critical.
cluster.delta = 200; % Overdensity cluster mass defined wrt.
cluster.redshift = 1.13e0;
cluster = findmasses(cluster, cosmology); % Converts cluster masses to m200m for rareness code

%% Specify settings
settings.masserr = 'normal'; % Can be 'normal' or 'lognormal'. If 'normal', error bars above should be symmetric!
settings.eddington_correct = 'n'; % Perform 'Eddington bias' correction on cluster mass?
settings.makeplots = 'y'; % Making plots takes extra computation, turn off to save time

%% Calculate rareness
[rareness, plots] = findrare(survey, cluster, settings, cosmology);

%% Draw a plot
create_contourfigure(plots, survey, cluster, 'grmdv'); % Type of contour plot can either be 'nu', 'grmgrz' or 'grmdv'
