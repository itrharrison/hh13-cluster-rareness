function cluster=findmasses(cl, cosm)
	
	cl.m = [cl.m cl.m_lower cl.m_upper];
	O_mz = cosm.O_m*((1.e0+cl.redshift).^3.e0)/( cosm.O_m*((1.e0+cl.redshift).^3.e0) + 1.e0 - cosm.O_m );

	if cl.delta == 200
		
		if strcmp(cl.rhotype, 'm')
			cl.m200m = cl.m;
		elseif strcmp(cl.rhotype, 'c')
			for j=1:length(cl.m)
				cl.m200c(j) = cl.m(j);
				cl.m200m(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 200.e0*O_mz);
				cl.m500c(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 500.e0);
				cl.m500m(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 500.e0*O_mz);
			end
		end
		
	elseif cl.delta == 500
		
		if strcmp(cl.rhotype, 'm')
			for j=1:length(cl.m)
				cl.m500m(j) = cl.m(j);
				cl.m200c(j) = m500m2m200c(cl.m500m(j), cl.redshift);
				cl.m200m(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 200.e0*O_mz);
				cl.m500c(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 500.e0);
			end
		elseif strcmp(cl.rhotype, 'c')
			for j=1:length(cl.m)
				cl.m500c(j) = cl.m(j);
				cl.m200c(j) = m500c2m200c(cl.m500c(j), cl.redshift);
				cl.m200m(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 200.e0*O_mz);
				cl.m500m(j) = m200c2mdelta(cl.m200c(j), cl.redshift, 500.e0*O_mz);
			end
		end
	
	end
	
	cl.m200m_lower = cl.m200m(2);
	cl.m200m_upper = cl.m200m(3);
	cl.m200m = cl.m200m(1);
	
	cluster = cl;
	
	function mret=m200c2mdelta(m, z, delta)
		c200 = c_m200_duffy(m, z);
		fn = @(x)(c200.^3)/(log(1+c200)-c200/(1+c200))*(log(1+x) - x/(1+x))/(x.^3) - delta/200;
		c500 = fzero(fn, [1, 10]);
		mret = m.*((c500./c200).^3.).*(delta/200);
	end

	function mret1=m500c2m200c(m, z)
		lnm200c_arr = linspace(log(1.e13), log(1.e16), 10);
		m200c_arr = exp(lnm200c_arr);
		m500c_arr = zeros(size(m200c_arr));
		
		for i=1:10
			m500c_arr(i) = m200c2mdelta(m200c_arr(i), z, 500);
		end
		
		lnm500c = interp1(log(m500c_arr), log(m200c_arr), log(m));
		
		mret1=exp(lnm500c);
	end

	function cret=c_m200_duffy(m, z)
		m_pivot = 2.e12;
		A = 5.71e0;
		B = -0.084e0;
		C = -0.47e0;
		
		cret = A.*((m/m_pivot).^B).*(1.+z).^C;
	end

end
