# README #

This is a README for the hh13-cluster-rareness code, which can be used for
calculating the rareness of extreme galaxy clusters, and reproducing the 
work of Harrison & Hothckiss 2013 JCAP07(2013)022 https://arxiv.org/abs/1210.4369

The code is currently implemented in MATLAB (sorry).

### Description ###

* The code calculates measures of rareness of a given galaxy cluster mass observation within a LCDM cosmological model
* For full descriptions of each rareness measure, see the above paper

### How do I get set up? ###

* See the example file for the list of parameters to be speciied and their description
* The rareness can then be calculated with:
	* [rareness, plots] = findrare(survey, cluster, settings, cosmology);
* And contour plots created with:
	* create_contourfigure(plots, survey, cluster, 'grmdv')

### Contact Details ###

* For any questions please contact either:
	* Ian Harrison (repo owner) http://orcid.org/0000-0002-4437-0770
	* Shaun Hotchkiss http://orcid.org/0000-0002-2027-3052