function create_contourfigure(plots, survey, cluster, type)

	if strcmp(type, 'nu')
		p = plots.nu;
	elseif strcmp(type, 'grmgrz')
		p = plots.grmgrz;
	elseif strcmp(type, 'grmdv')
		p = plots.grmdv;
	end
	
	% Create figure
	figure1 = figure('PaperUnits','centimeters',...
		'PaperSize',[0.6 0.6],...
		'InvertHardcopy','off');

	% Create axes
	axes1 = axes('Parent',figure1,...
		'FontSize',14,...
		'FontName','times');
	
	xlim(axes1,[survey.z_min survey.z_max]);
	ylim(axes1,[log10(survey.m_min) log10(survey.m_max)]);
	
	box(axes1,'on');
	hold(axes1,'all');

	% Create image
	contour(plots.z_arr,log10(plots.m_arr),p.Pgtrm0.data/max(max(p.Pgtrm0.data)),[0.66, 0.95, 0.997],'LineWidth',1,'ShowText', 'on');
	errorbar(cluster.redshift, log10(plots.m_plot), (cluster.m200m_lower/(plots.m_plot*log(10))), (cluster.m200m_lower/(plots.m_plot*log(10))), 'p');

	% Create xlabel
	xlabel('$z$','Interpreter','latex','FontSize',14,'FontName','times');

	% Create ylabel
	ylabel('Mass $\left[\log_{10}(M_\odot/h)\right]$','Interpreter','latex',...
		'FontSize',14,...
		'FontName','times');

end
